import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(search: string, page :number, pageSize: number) {
        const people_filtered = people_data
                                .filter(people => !search || 
                                    (
                                        people.title?.includes(search) || 
                                        people.gender?.includes(search) || 
                                        people.company?.includes(search)
                                    )
                                )
        return {
            people: people_filtered.slice(page * pageSize, (page * pageSize) + pageSize),
            total: people_filtered.length
        };
    }
}
